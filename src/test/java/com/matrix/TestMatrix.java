package com.matrix;

import org.junit.jupiter.api.Test;

import com.matrix.Matrix;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TestMatrix {
	
	@Test
	public void testInitialization() {
		assertThrows(Exception.class, () -> {
			new Matrix(0, 3);
		});
		
		assertThrows(Exception.class, () -> {
			new Matrix(3, 0);
		});
		
		assertThrows(Exception.class, () -> {
			new Matrix(-1, 10);
		});
		
		assertThrows(Exception.class, () -> {
			new Matrix(10, -1);
		});
		
		assertDoesNotThrow( () -> {
			new Matrix(3, 3);
		});
	}
	
	@Test
	public void testData() throws Exception {
		Matrix m = new Matrix(3, 4);
		
		assertNotNull(m.getData());
		
		assertEquals(3, m.getRows());
		assertEquals(4, m.getColumns());
		
		assertDoesNotThrow( () -> {
			m.setData(2, 2, 20);
		});
		
		String str = m.toString();
		assertNotNull(str);
		
		assertThrows(Exception.class, () -> {
			m.setData(-1, 10, 10);
		});
		
		assertThrows(Exception.class, () -> {
			m.setData(10, -1, 10);
		});
		
		assertThrows(Exception.class, () -> {
			m.setData(3, 1, 10);
		});
		
		assertThrows(Exception.class, () -> {
			m.setData(1, 4, 10);
		});
		
		assertThrows(Exception.class, () -> {
			m.setData(4, 2, 10);
		});
		
		assertThrows(Exception.class, () -> {
			m.setData(2, 5, 20);
		});
		
		
		
		assertDoesNotThrow( () -> {
			m.getData(1, 2);
		});
		
		assertThrows(Exception.class, () -> {
			m.getData(-1, 1);
		});
		
		assertThrows(Exception.class, () -> {
			m.getData(1, -1);
		});
		
		assertThrows(Exception.class, () -> {
			m.getData(3, 1);
		});
		
		assertThrows(Exception.class, () -> {
			m.getData(1, 4);
		});
		
		assertThrows(Exception.class, () -> {
			m.getData(4, 1);
		});
		
		assertThrows(Exception.class, () -> {
			m.getData(1, 5);
		});
	}

	@Test
	public void testMultiplication() throws Exception {
		Matrix m1 = new Matrix(4, 2);
		m1.setData(0, 0, -2);
		m1.setData(0, 1, 2);
		m1.setData(1, 0, -3);
		m1.setData(1, 1, 3);
		m1.setData(2, 0, 4);
		m1.setData(2, 1, 6);
		m1.setData(3, 0, 5);
		m1.setData(3, 1, 7);

		Matrix m2 = new Matrix(2, 3);
		m2.setData(0, 0, -2);
		m2.setData(0, 1, 8);
		m2.setData(0, 2, -4);
		m2.setData(1, 0, -3);
		m2.setData(1, 1, 1);
		m2.setData(1, 2, -1);
		
		Matrix m = MatrixOperator.multiply(m1, m2);
		
		assertEquals(m.getRows(), 4);
		assertEquals(m.getColumns(), 3);
		assertEquals(m.getData(0, 0), -2);
		assertEquals(m.getData(0, 1), -14);
		assertEquals(m.getData(0, 2), 6);
		assertEquals(m.getData(1, 0), -3);
		assertEquals(m.getData(1, 1), -21);
		assertEquals(m.getData(1, 2), 9);
		assertEquals(m.getData(2, 0), -26);
		assertEquals(m.getData(2, 1), 38);
		assertEquals(m.getData(2, 2), -22);
		assertEquals(m.getData(3, 0), -31);
		assertEquals(m.getData(3, 1), 47);
		assertEquals(m.getData(3, 2), -27);
	}

}
