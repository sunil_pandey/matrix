package com.matrix;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utility class with matrix operation functions
 * @author sunil_pandey
 */
public class MatrixOperator {
	
	/**
	 * Logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(MatrixOperator.class);

	/**
	 * Multiplies two matrixes
	 * @param m1 the first matrix to multiply
	 * @param m2 the second matrix (the matrix to be multiplied to)
	 * @return the resultant matrix that is the product of matrices <code>m1</code> and <code>m2</code>
	 * @throws Exception if any error occurs
	 */
	public static Matrix multiply(Matrix m1, Matrix m2) throws Exception {
		int rows1 = m1.getRows();
		int columns1 = m1.getColumns();
		double[][] data1 = m1.getData();
		int rows2 = m2.getRows();
		int columns2 = m2.getColumns();
		double[][] data2 = m2.getData();
		
		if (columns1 != rows2) {
			throw new Exception ("Dimensions do not match, number of columns in first matrix = " + columns1 + ", but numbemr of rows in second matrix = " + rows2);
		}
		double[][] data = new double[rows1][columns2];
		for (int i = 0; i < rows1; ++i) {
			for (int j = 0; j < columns2; ++j) {
				data[i][j] = 0;
				for (int k = 0; k < columns1; ++k) {
					data[i][j] += (data1[i][k] * data2[k][j]);
				}
			}
		}
		return new Matrix(rows1, columns2, data);
	}
	
	public static void main(String[] args) {
		try {
			Matrix m1 = new Matrix(4, 2);
			m1.setData(0, 0, -2);
			m1.setData(0, 1, 2);
			m1.setData(1, 0, -3);
			m1.setData(1, 1, 3);
			m1.setData(2, 0, 4);
			m1.setData(2, 1, 6);
			m1.setData(3, 0, 5);
			m1.setData(3, 1, 7);
			LOGGER.debug("Matrix 1\n" + m1.toString());

			Matrix m2 = new Matrix(2, 3);
			m2.setData(0, 0, -2);
			m2.setData(0, 1, 8);
			m2.setData(0, 2, -4);
			m2.setData(1, 0, -3);
			m2.setData(1, 1, 1);
			m2.setData(1, 2, -1);
			LOGGER.debug("Matrix 2\n" + m2.toString());
			
			Matrix m = multiply(m1, m2);
			LOGGER.debug("RESULT\n" + m.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
}
