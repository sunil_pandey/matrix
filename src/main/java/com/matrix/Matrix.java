package com.matrix;

/**
 * This class denotes a matrix
 * @author sunil_pandey
 */
public class Matrix {
	
	/**
	 * Number of rows in the matrix
	 */
	private int rows;
	
	/**
	 * Number of columns in the matrix
	 */
	private int columns;
	
	/**
	 * Data in matrix
	 */
	private double[][] data;
	
	/**
	 * Constructor
	 * @param rows number of rows in the matrix
	 * @param columns number of columns in the matrix
	 * @throws Exception if the number of rows / columns specified is not positive and greater than zero
	 */
	public Matrix(final int rows, final int columns) throws Exception {
		if (rows <= 0) {
			throw new Exception("Number of rows must be greater than zero");
		}
		if (columns <= 0) {
			throw new Exception("Number of columns must be greater than zero");
		}
		this.rows = rows;
		this.columns = columns;
		data = new double[rows][columns];
	}
	
	/**
	 * Constructor
	 * @param rows number of rows in the matrix
	 * @param columns number of columns in the matrix
	 * @param data the data for the matrix
	 * @throws Exception if the number of rows / columns specified is not positive and greater than zero
	 */
	public Matrix (final int rows, final int columns, final double[][] data) throws Exception {
		this(rows, columns);
		if (data != null) {
			this.data = data;
		}
	}
	
	/**
	 * Sets data of a particular cell
	 * @param row the row in which to be set, row# starts with zero
	 * @param column the column in which to be set, column# starts with zero
	 * @param data the data to be set in the specified element at the specified row and column
	 * @throws Exception if row / column specified is invalid (negative or a value higher than the matrix can hold)
	 */
	public void setData(int row, int column, double data) throws Exception {
		checkRowColumn(row, column);
		this.data[row][column] = data;
	}
	
	/**
	 * Gets data from the specified location
	 * @param row the row from which data is to be fetched, row# starts with zero
	 * @param column the column from which data is to be fetched, column# start with zero 
	 * @return the data at the specified row and column
	 */
	public double getData(int row, int column) throws Exception {
		checkRowColumn(row, column);
		return data[row][column];
	}
	
	/**
	 * Gets the number of rows in the matrix
	 * @return the number of rows in the matrix
	 */
	public int getRows() {
		return rows;
	}

	/**
	 * Gets the number of columns in the matrix
	 * @return the number of columns in the matrix
	 */
	public int getColumns() {
		return columns;
	}

	/**
	 * Gets the entire data  in the matrix
	 * @return the entire data  in the matrix
	 */
	public double[][] getData() {
		return data;
	}

	/**
	 * Matrix is provided as String in a tabular format
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < rows; ++i) {
			for (int j = 0; j < columns; ++j) {
				sb.append("\t");
				sb.append(data[i][j]);
			}
			sb.append("\n");
		}
		return sb.toString();
	}
	
	private void checkRowColumn(int row, int column) throws Exception {
		if (row < 0) {
			throw new Exception("Row cannot be negative");
		}
		if (column < 0) {
			throw new Exception("Column cannot be negative");
		}
		if (row >= rows) {
			throw new Exception("Row is wrong, row given " + row + " must be lesser than " + rows);
		}
		if (column > columns) {
			throw new Exception("Column is wrong, column given " + column + " must be lesser than " + columns);
		}
	}
	
}
