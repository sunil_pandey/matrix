# Matrix
Matrix Operations

Currently, this supports matrix multiplication, we can add support of other operations as we go ahead.
This is a plain Java application that does matrix multiplication.
The operator class has a main method that demonstrates the functionality of the application.

## How to build, run, test

1. Install latest version of gradle.

2. Execute the command: "gradle clean build".

3. To see a demo run, execute the command: "gradle run".

4. To run test cases, execute the command: "gradle test".
